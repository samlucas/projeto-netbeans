package classes;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.faces.context.FacesContext;

@ManagedBean
@ViewScoped
public class AlunoBean_3 implements Serializable {

    private static final long serialVersionUID = 1L;
    private String nome;
    private Boolean aceito;

    public void cadastrar() {
        System.out.println("Nome:" + this.nome);
        System.out.println("Aceito: " + this.aceito);
        FacesContext.getCurrentInstance().addMessage(null, new FacesMessage("Cadastro realizado!"));
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public Boolean getAceito() {
        return aceito;
    }

    public void setAceito(Boolean aceito) {
        this.aceito = aceito;
    }

}
